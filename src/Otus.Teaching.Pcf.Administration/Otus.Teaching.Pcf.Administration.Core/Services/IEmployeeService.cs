﻿namespace Otus.Teaching.Pcf.Administration.Core.Services
{
    using System;
    using System.Threading.Tasks;

    public interface IEmployeeService
    {
        Task<bool> UpdateAppliedPromocodesAsync(Guid id);
    }
}