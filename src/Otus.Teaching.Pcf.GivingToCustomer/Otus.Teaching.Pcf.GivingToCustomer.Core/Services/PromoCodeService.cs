﻿namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
    using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
    using Otus.Teaching.Pcf.Messaging.CrossSystemContracts;

    public class PromoCodeService : IPromoCodeService
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Customer> _customersRepository;
        private readonly IRepository<Preference> _preferencesRepository;

        public PromoCodeService(
            IRepository<PromoCode> promoCodesRepository,
            IRepository<Customer> customersRepository,
            IRepository<Preference> preferencesRepository)
        {
            this._promoCodesRepository = promoCodesRepository;
            this._customersRepository = customersRepository;
            this._preferencesRepository = preferencesRepository;
        }

        public async Task<bool> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference = await this._preferencesRepository.GetByIdAsync(request.PreferenceId);

            if (preference is null)
            {
                throw new ArgumentException($"Предпочтения с id = \"{request.PreferenceId}\" не существует!");
            }

            //  Получаем клиентов с этим предпочтением:
            var customers = (await this._customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id))).ToList();

            if (!customers.Any())
                throw new Exception($"Не найдено ни одного клиента с предпочтением \"{preference.Name}\"!");

            var promoCode = new PromoCode
            {
                Id = request.PromoCodeId,
                PartnerId = request.PartnerId,
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                BeginDate = DateTime.Parse(request.BeginDate),
                EndDate = DateTime.Parse(request.EndDate),
                Preference = preference,
                PreferenceId = preference.Id,
                Customers = new List<PromoCodeCustomer>()
            };

            foreach (var item in customers)
            {
                promoCode.Customers.Add(new PromoCodeCustomer()
                {
                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = promoCode.Id,
                    PromoCode = promoCode
                });
            };

            await this._promoCodesRepository.AddAsync(promoCode);

            return true;
        }
    }
}
