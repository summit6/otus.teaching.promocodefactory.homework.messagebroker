﻿namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    using System.Threading.Tasks;
    using Otus.Teaching.Pcf.Messaging.CrossSystemContracts;

    public interface IPromoCodeService
    {
        Task<bool> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request);
    }
}