﻿namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Messaging
{
    using System.Threading.Tasks;
    using MassTransit;
    using Otus.Teaching.Pcf.GivingToCustomer.Core.Services;
    using Otus.Teaching.Pcf.Messaging.CrossSystemContracts;

    public class GivePromoCodeToCustomerMessageConsumer : IConsumer<GivePromoCodeRequest>
    {
        private readonly IPromoCodeService _promoCodeService;

        public GivePromoCodeToCustomerMessageConsumer(IPromoCodeService promoCodeService)
        {
            this._promoCodeService = promoCodeService;
        }

        public async Task Consume(ConsumeContext<GivePromoCodeRequest> context)
        {
            await this._promoCodeService.GivePromoCodesToCustomersWithPreferenceAsync(context.Message);
        }
    }
}
